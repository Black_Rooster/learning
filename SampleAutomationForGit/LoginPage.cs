﻿using OpenQA.Selenium;

namespace SampleAutomationForGit
{
    public class LoginPage : BasePage
    {
        public LoginPage(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement UsernameField => Driver.FindElement(By.Name("Username"));

        public IWebElement PasswordField => Driver.FindElement(By.Name("Password"));

        public IWebElement LoginButton => Driver.FindElement(By.Name("LoginButton"));

        public IWebElement ErrorMessage => Driver.FindElement(By.Id("ctl03_divAccessDenied"));
    }
}
