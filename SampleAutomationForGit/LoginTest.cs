﻿using NUnit.Framework;

namespace SampleAutomationForGit
{
    [TestFixture]
    public class LoginTest : BaseTest
    {
        [Test]
        public void GivenValidLoginCredentials_WhenLogginIn_ThenConfirmTheUserIsDirectToLandingPage()
        {
            MyLexisNexis.LoginPage.UsernameField.SendKeys("gregc");
            MyLexisNexis.LoginPage.PasswordField.SendKeys("gregc");
            MyLexisNexis.LoginPage.LoginButton.Click();

            Assert.AreEqual("https://www.mylexisnexis.co.za/Index.aspx", MyLexisNexis.LoginPage.CurrentURL);
        }

        [Test]
        public void GivenInValidLognCredentials_WhenLogginIn_ThenConfirmValidationMessageShow()
        {
            MyLexisNexis.LoginPage.UsernameField.SendKeys("gregc1");
            MyLexisNexis.LoginPage.PasswordField.SendKeys("gregc123");
            MyLexisNexis.LoginPage.LoginButton.Click();

            Assert.IsTrue(MyLexisNexis.LoginPage.ErrorMessage.Displayed);
        }
    }
}
