﻿using OpenQA.Selenium;

namespace SampleAutomationForGit
{
    public static class MyLexisNexis
    {
        public static BasePage BasePage;
        public static LoginPage LoginPage;

        public static void PageInitializer(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("https://www.mylexisnexis.co.za");
            BasePage = new BasePage(driver);
            LoginPage = new LoginPage(driver);
        }
    }
}
