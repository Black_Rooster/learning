﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace SampleAutomationForGit
{
    public class BasePage
    {
        public WebDriverWait DriverWait;
        public IWebDriver Driver;

        public BasePage(IWebDriver driver)
        {
            Driver = driver;
            DriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
        }

        public string CurrentURL => Driver.Url;
    }
}
