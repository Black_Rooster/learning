﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace SampleAutomationForGit
{
    [TestFixture]
    public class BaseTest
    {
        private IWebDriver _driver;

        [SetUp]
        public void DriverSetUp()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            MyLexisNexis.PageInitializer(_driver);
        }

        [TearDown]
        public void DriverShutdown()
        {
            _driver.Dispose();
        }
    }
}
